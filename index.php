<?php
$ch = curl_init();
curl_setopt( $ch, CURLOPT_URL, 'https://www.digikala.com/product/dkp-90825');
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
$content = curl_exec($ch);
$dom = new DOMDocument();
@$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
$finder = new DomXPath($dom);

//for getting price
$classname1="c-product__seller-price-pure js-price-value";
$expression1 = "//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname1 ')]";

//for getting main image
$classname2="js-gallery-img";
$expression2 = "//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname2 ')]";
$price = $image = '';
foreach ($finder->evaluate($expression1) as $div) {
    $price = $div->textContent;
}
foreach ($finder->evaluate($expression2) as $img) {
    $image = $img->getAttribute('data-src');
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>test</title>
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <style type="text/css">
        .btn{
            background-color: #ef394e;
            border: 0;
            margin: 11px;
            width: 80%;
            height: 13%;
            border-radius: 9px;
            font-size: 1vw;
            color: white;cursor: pointer;
        }
        .price{
            font-size: 2vw;
            color: #ef394e;
        }
        .img{
            width: 80%;
            height: 60%;
        }
        .main{
            border: solid 2px;
            width: 25%;
            height: 80%;
            display: flex;
            border-color: #e4e4e4;
            flex-direction: column;
            align-items: center;
        }
        </style>
    </head>
    <body>
        <div class = 'main'>
            <img class='img' src = "<?= $image ?>" />
            <div class='price'><?= $price ?> تومان </div>
            <button type='button' class='btn'>افزودن به سبد خرید</button>
        </div>
    </body>
</html>
